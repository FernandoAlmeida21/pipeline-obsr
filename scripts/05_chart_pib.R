box::use(
  pool[dbPool, poolClose],DBI, glue[glue],
  RPostgres[Postgres, dbListTables, dbGetQuery], 
  dplyr, readr[parse_number], echarts4r
)

# conexao com o banco de dados
obsr <- dbPool(
  drv = Postgres(),
  dbname = "obsr",
  host = "172.31.3.30",
  user = "shiny",
  password = "hg9WkedAJyp7uFpoZbSX"
)

# listando tabelas do banco de dados
tabelas_banco <- dbListTables(obsr)

# filtrando apenas tabelas de pesagens dos caminhoes do aterro sanitario
padrao <- "relatorio_pesagem"
relatorios_pesagem <- grep(padrao, tabelas_banco, value = TRUE)

# criando dataframe que recebera ano e volume total da coleta domiciliar em toneladas
df_pesagens <- data.frame(matrix(nrow=0, ncol=length(columns)))

for (table in relatorios_pesagem){
  # extraindo ano e peso liquido total do ano referente
  ano <- parse_number(table)
  volume <- dbGetQuery(obsr, sprintf("SELECT SUM(peso_liquido::int) FROM %s", table))
  
  # adicionando dados a df_pesagens
  df_pesagens <- rbind(df_pesagens, list(ano, (volume[1][1]/1000)))
}

# encerrando conexao com banco de dados
poolClose(obsr)

# renomeando colunas de df_pesagens
columns <- c('ano', 'peso_toneladas')
colnames(df_pesagens) <- columns

# ordenando df_pesagens pelo ano
df_pesagens$ano <- sapply(df_pesagens$ano, as.numeric)
df_pesagens <- df_pesagens[order(df_pesagens$ano, decreasing=FALSE),]

# adicionando dados de PIB
df_pib <- read.csv('~/Documents/PipelineOBSR/pipeline-obsr/raw data/PIB.csv',
                   col.names = c('ano','pib_corrente_fortaleza'))

# inner join entre pesagens de residuos e PIB
df <- dplyr::inner_join(df_pesagens, df_pib, by='ano')

# dividindo por 1.000.000
df$peso_toneladas <- as.numeric(df$peso_toneladas)/1000000
df$pib_corrente_fortaleza <- as.numeric(df$pib_corrente_fortaleza)/1000000

# label que remove virgula dos anos no eixo x
label <- list(
  formatter = htmlwidgets::JS(
    'function(value, index){
            return value;
        }'
  )
)

# serie historica do PIB e coleta de residuos
df |>
  e_charts(ano) |>
  e_line(pib_corrente_fortaleza, name='PIB') |>
  e_line(peso_toneladas, y_index=1, name='Volume de Resíduos Sólidos') |>
  e_x_axis(serie=ano, axisLabel=label) |>
  e_theme('caravan') |>
  e_y_axis(splitLine = list(show = FALSE)) |>
  e_tooltip(trigger = "axis", 
            backgroundColor = 'rgba(255,255,255,0.7)',
            style='decimal')

# calculo da correlacao de Pearson
corr <- cor(df$pib_corrente_fortaleza,df$peso_toneladas, method='pearson')

# scatter plot do PIB e coleta de residuos
df |>
  e_charts(pib_corrente_fortaleza) |>
  e_scatter(peso_toneladas, symbol_size=6) |>
  e_legend(FALSE) |> 
  e_x_axis(min=50, max=80, axisLabel=label) |>
  e_lm(peso_toneladas ~ pib_corrente_fortaleza, name = "y = ax + b") |>
  e_theme_custom('{"color":["#ff715e","#ffaf51"]}') |>
  e_title('PIB vs. Volume de Resíduos Sólidos', paste("Correlação de Pearson:",round(corr, digits=2)))
