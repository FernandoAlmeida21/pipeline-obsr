import time
import pandas as pd
import configparser
from selenium import webdriver
from datetime import datetime, timedelta
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# enviroment variables
parser = configparser.ConfigParser()
parser.read('../credentials.conf')
url_edinheiro  = parser.get('edinheiro_credentials', 'url')
username_edinheiro = parser.get('edinheiro_credentials', 'username')
password_edinheiro = parser.get('edinheiro_credentials', 'password')
download_temp = '/Users/pedroflorencio/Documents/PipelineOBSR/pipeline-obsr/raw data'

# O.S. profile settings
profile = webdriver.FirefoxProfile()
profile.set_preference("browser.download.folderList", 2)
profile.set_preference("browser.download.dir", download_temp)
profile.set_preference("browser.download.useDownloadDir", True)
profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/octet-stream")

# options
options = webdriver.FirefoxOptions()
options.profile = profile
#options.add_argument('--headless')
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')

# inicializating the webpage
driver = webdriver.Firefox(options=options)
driver.get(url_edinheiro)

# performing login on the page
driver.find_element(By.ID, "email").send_keys(username_edinheiro)
driver.find_element(By.NAME, "password").send_keys(password_edinheiro)
driver.find_element(By.ID, 'select2-ecoponto-container').click()
driver.find_element(By.XPATH, "//ul[@id='select2-ecoponto-results']/li[text()='ECOPONTO BARRA DO CEARÁ']").click()
driver.find_element(By.XPATH, "//div[@class='form-actions']/button[@class='btn green pull-right']").click()
time.sleep(8)

# clicking on "Transações por Período"
driver.find_element(By.XPATH, "//span[contains(., 'Resumo do Projeto')]").click()
time.sleep(5)
driver.find_element(By.XPATH, "//span[contains(., 'Transações por Período')]").click()

# selecting the period
today = datetime.now()
yesterday = today - timedelta(days=1)
date = yesterday.strftime('%d/%m/%Y')
driver.find_element(By.CSS_SELECTOR, "input[name='to']").clear()
driver.find_element(By.CSS_SELECTOR, "input[name='to']").send_keys(date)
time.sleep(3)
driver.find_element(By.CSS_SELECTOR, "input[name='from']").clear()
driver.find_element(By.CSS_SELECTOR, "input[name='from']").send_keys(date)
time.sleep(3)
driver.find_element(By.XPATH, "//span[contains(., 'até')]").click()

# report generation
driver.find_element(By.XPATH, "//div[@class='col-md-offset-3 col-md-9']/button[@class='btn green']").click()