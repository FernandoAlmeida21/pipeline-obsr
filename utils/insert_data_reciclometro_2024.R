box::use(
  pool[dbPool, poolClose],DBI, glue[glue],
  RPostgres[Postgres], dplyr
)

obsr <- dbPool(
  drv = Postgres(),
  dbname = "obsr",
  host = "172.31.3.30",
  user = "shiny",
  password = "hg9WkedAJyp7uFpoZbSX"
)

data <- utils::read.csv("new_data.csv")
data <- as.data.frame(data)

data <- data[-c(1),]

colnames(data) <- c("idoperacao", "status", "data", "ecoponto", "operador", "municipe", "tipo_municipe","tipo",
                    "residuo", "quantidade_kg", "subtotal_rs", "total_rs")

data$quantidade_kg <- gsub(",", ".", data$quantidade_kg)
data$subtotal_rs <- gsub(",", ".", data$subtotal_rs)
data$total_rs <- gsub(",", ".", data$total_rs)

utils::write.csv(data, "data_etl.csv", row.names=FALSE)

DBI::dbAppendTable(obsr, "br_ecofor_ecoponto_2024", data)

poolClose(obsr)
